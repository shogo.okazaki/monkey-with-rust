#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Token {
    Illegal,
    EOF,

    // 識別子 + リテラル
    Ident(String),
    Int(i32),

    // 演算子
    Assign,
    Plus,
    Minus,
    Bang,
    Asterisk,
    Slash,

    Lt,
    Gt,

    // デリミタ
    Comma,
    Semicolon,

    Lparen,
    Rparen,
    Lbrace,
    Rbrace,

    // キーワード
    Function,
    Let
}

impl Token {

    pub fn lookup_keyword(ident: &str) -> Option<Token> {
        match ident {
            "fn" => Some(Token::Function),
            "let" => Some(Token::Let),
            _ => None,
        }
    }
}