use crate::token::Token;

pub struct Lexer<'a> {
    input: std::str::Chars<'a>,
    cur: char,
    peek: char,
}

impl<'a> Lexer<'a> {
    /// 入力ソースを受け取り、Lexer インスタンスを返却する
    pub fn new(input: &'a str) -> Self {
        let mut lexer = Lexer {
            input: input.chars(),
            cur: '\u{0}',
            peek: '\u{0}'
        };
        lexer.read_char();
        lexer.read_char();
        lexer
    }

    /// 解析文字を 1 文字進める
    fn read_char(&mut self) -> char {
        let c = self.cur;
        self.cur = self.peek;
        self.peek = self.input.next().unwrap_or('\u{0}');
        c
    }

    /// 検査中の文字に応じた Token を返す
    fn next_token(&mut self) -> Token {
        self.skip_whitespace();

        let token = match self.cur {
            '=' => Token::Assign,
            '!' => Token::Bang,
            '(' => Token::Lparen,
            ')' => Token::Rparen,
            '{' => Token::Lbrace,
            '}' => Token::Rbrace,
            '+' => Token::Plus,
            '-' => Token::Minus,
            '/' => Token::Slash,
            '*' => Token::Asterisk,
            '<' => Token::Lt,
            '>' => Token::Gt,
            ';' => Token::Semicolon,
            ',' => Token::Comma,
            '\u{0}' => Token::EOF,
            c => {
                if is_letter(c) {
                    return self.read_identifier();
                } else if c.is_ascii_digit() {
                    return self.read_number();
                } else {
                    Token::Illegal
                }
            }
        };
        self.read_char();
        token
    }

    /// 識別子を読み、非英字に達するまで字句解析を進める
    fn read_identifier(&mut self) -> Token {
        let mut ident = String::new();
        while is_letter(self.cur) {
            ident.push(self.read_char());
        }

        if let Some(token) = Token::lookup_keyword(&ident) {
            token
        } else {
            Token::Ident(ident)
        }
    }

    fn read_number(&mut self) -> Token {
        let mut num = String::new();
        while self.cur.is_ascii_digit() {
            num.push(self.read_char());
        }
        Token::Int(num.parse().unwrap())
    }

    /// 空白文字をスキップする
    fn skip_whitespace(&mut self) {
        while self.cur == ' ' || self.cur == '\t' || self.cur == '\n' || self.cur == '\r' {
            self.read_char();
        }
    }

 }

fn is_letter(ch: char) -> bool {
    'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_'
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_next_token(input: &str, tests: Vec<Token>) {
        let mut lexer = Lexer::new(input);
        for expectedToken in tests {
            let token = lexer.next_token();
            assert_eq!(token, expectedToken);
        }
    }

    #[test]
    fn test_for_short_input() {
        let input = "=+(){};,";
        let test_cases = vec![
            Token::Assign,
            Token::Plus,
            Token::Lparen,
            Token::Rparen,
            Token::Lbrace,
            Token::Rbrace,
            Token::Semicolon,
            Token::Comma,
            Token::EOF,
        ];

        test_next_token(input, test_cases);
    }

    #[test]
    fn test_for_short_code() {
        let input = r#"let five = 5;
        let ten = 10;

        let add = fn(x, y) {
          x + y;
        };

        let result = add(five, ten);
        !-/*5;
        5 < 10 > 5;
        "#;

        let test_cases = vec![
            Token::Let,
            Token::Ident("five".to_owned()),
            Token::Assign,
            Token::Int(5),
            Token::Semicolon,
            Token::Let,
            Token::Ident("ten".to_owned()),
            Token::Assign,
            Token::Int(10),
            Token::Semicolon,
            Token::Let,
            Token::Ident("add".to_owned()),
            Token::Assign,
            Token::Function,
            Token::Lparen,
            Token::Ident("x".to_owned()),
            Token::Comma,
            Token::Ident("y".to_owned()),
            Token::Rparen,
            Token::Lbrace,
            Token::Ident("x".to_owned()),
            Token::Plus,
            Token::Ident("y".to_owned()),
            Token::Semicolon,
            Token::Rbrace,
            Token::Semicolon,
            Token::Let,
            Token::Ident("result".to_owned()),
            Token::Assign,
            Token::Ident("add".to_owned()),
            Token::Lparen,
            Token::Ident("five".to_owned()),
            Token::Comma,
            Token::Ident("ten".to_owned()),
            Token::Rparen,
            Token::Semicolon,
            Token::Bang,
            Token::Minus,
            Token::Slash,
            Token::Asterisk,
            Token::Int(5),
            Token::Semicolon,
            Token::Int(5),
            Token::Lt,
            Token::Int(10),
            Token::Gt,
            Token::Int(5),
            Token::Semicolon,
            Token::EOF,
        ];

        test_next_token(input, test_cases);
    }
}